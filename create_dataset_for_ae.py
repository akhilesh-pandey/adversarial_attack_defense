from attacks import  fgsm
from utils import save_model, load_model, get_dataloader, get_customDataLoader
import torch.utils.data as data_utils

import torch
import torch.nn as nn
import torch.optim as optim
from model import FC_model, AE


device = ('cuda' if torch.cuda.is_available() else 'cpu')

batch_size= 200
train_loader, test_loader = get_dataloader(batch_size)

model= FC_model()
load_model(model=model, model_path='./fc_model_100_100_10.pth')

criterion= nn.CrossEntropyLoss()
optimizer= optim.Adam(model.parameters(), lr= 0.001)

model.to(device)

eps = 0.25

adv_train_data_list = []
clean_train_data_list = []
train_label_list = []

for i, (data, label) in enumerate(train_loader):
    clean_train_data_list.append(data)
    size = data.size()
    adv_train_data_list.append(fgsm(model,data.view(-1,size[-1]*size[-2]), label, epsilon=eps, loss_fn=criterion).view(size))
    train_label_list.append(data)


clean_test_data_list = []
adv_test_data_list= []
test_label_list = []

for i, (data, label) in enumerate(test_loader):
    clean_test_data_list.append(data)
    size= data.size()
    adv_test_data_list.append(fgsm(model, data.view(-1,size[-1]*size[-2]), label, epsilon=eps, loss_fn= criterion).view(size))
    test_label_list.append(data)# Label for autoencoders are clean images
#
clean_train_data_tensor = torch.cat(clean_train_data_list, 0)
adv_train_data_tensor = torch.cat(adv_train_data_list, 0)
train_label_tensor = torch.cat(train_label_list, 0)

clean_test_data_tensor = torch.cat(clean_test_data_list, 0)
adv_test_data_tensor = torch.cat(adv_test_data_list, 0)
test_label_tensor = torch.cat(test_label_list, 0)

#
# # reshape the data because our model is fully connected
# clean_train_data_tensor = clean_train_data_tensor.view(-1, 28*28)
# clean_test_data_tensor = clean_test_data_tensor.view(-1, 28*28)
#
# adv_train_x = fgsm(model, clean_train_data_tensor, train_label_tensor, loss_fn= criterion, epsilon=eps)
# adv_test_x = fgsm(model, clean_test_data_tensor, test_label_tensor, loss_fn= criterion, epsilon= eps)
#
total_train_data = torch.cat([clean_train_data_tensor, adv_train_data_tensor],0)
total_test_data = torch.cat([clean_test_data_tensor, adv_test_data_tensor],0)
#
total_train_label = torch.cat([train_label_tensor, train_label_tensor], 0)
total_test_label = torch.cat([test_label_tensor, test_label_tensor], 0)
complete_data = {'total_train_data': total_train_data, 'total_train_label': total_train_label,
                  'total_test_data': total_test_data, 'total_test_label': total_test_label}

torch.save(complete_data, 'data_for_autoencoder.pth')


