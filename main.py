
from utils import get_dataloader
from model import AE, FC_model, AE_shallow
# from CAE import CAE
# from utils import save_model, get_dataloader, load_model, get_customDataLoader
# from attacks import fgsm
import torch
import torch.nn as nn
import torch.optim as optim
from trainer import Trainer



# def train_base_model(model, train_loader, test_loader,
#                      save_path='./fc_model_100_100_10.pth'):
#     epochs = 1000
#     best_acc = 0
#     for epoch in range(epochs):
#         train(model, epoch, train_loader)
#         val_acc = test(model, test_loader)
#         if best_acc < val_acc:
#             best_acc = val_acc
#             save_model(epoch, model, best_acc, optimizer, model_path=save_path)
#     return
#
#
# def train(model, epoch, train_loader):
#     model.train()
#     loss_list, batch_list = [], []
#     for i, (images, labels) in enumerate(train_loader):
#         images= images.view(-1, 28*28)
#         images = images.to(device)
#         labels = labels.to(device)
#         optimizer.zero_grad()
#         output = model(images)
#         loss = criterion(output, labels)
#         loss_list.append(loss.item())
#         batch_list.append(i + 1)
#
#         if i + 1 % 100 == 0:
#             print('Train epoch: {}, Batch: {}, loss: {}'.format(epoch, i, loss.item()))
#         loss.backward()
#         optimizer.step()
#
#
# def test(model, test_loader):
#     model.eval()
#     correct, total = 0, 0
#     with torch.no_grad():
#         for images, labels in test_loader:
#             images = images.to(device)
#             labels = labels.to(device)
#             images= images.view(-1, 28*28)
#             _, images = ae_model(images)
#             outputs = model(images)
#             _, predicted = torch.max(outputs.data, 1)
#             correct += (predicted == labels).sum().item()
#             total += labels.size(0)
#
#         print('Accuracy on testset = {:.2%}'.format(correct/total))
#         return correct/total
#
# def adv_attack(model,defender, test_loader):
#     model.eval()
#     correct, total = 0, 0
#     for images, labels in test_loader:
#         images = images.to(device)
#         labels = labels.to(device)
#         images= images.view(-1, 28*28)
#         images= fgsm(model, images, labels, epsilon=0.45, loss_fn=criterion)
#         _,images= defender(images) # using AE
#         outputs = model(images)
#         _, predicted = torch.max(outputs.data, 1)
#         correct += (predicted == labels).sum().item()
#         total += labels.size(0)
#
#     print('Accuracy on ADV_TESTSET = {:.2%}'.format(correct/total))
#     return correct/total



def main():
    epochs = 200
    batch_size = 200
    train_loader, test_loader = get_dataloader(batch_size)

    ae_model = AE()
    ae_shallow = AE
    fc_model = FC_model()
    optimizer = torch.optim.Adam(fc_model.parameters(), lr=0.001)

    train_loader, test_loader = get_dataloader(256)

    # trainer = Trainer(fc_model, optimizer, nn.CrossEntropyLoss(), save_dir ='./fc_78_100_100_10.pth')
    trainer = Trainer(ae_shallow, optimizer, nn.MSELoss(), save_dir='./ae_784_80_784.pth')
    trainer.loop(epochs, train_loader, test_loader)

if __name__ == '__main__':
    main()
