import torch

# from tqdm import tqdm


class Trainer(object):
    torch.backends.cudnn.benchmarks = True

    def __init__(self, model, optimizer, loss_f, save_dir='.',
                 pretrained=False):

        self.device = ('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = model
        #self.model.to(self.device)  # import the model to GPU is available
        self.optimizer = optimizer
        self.loss_f = loss_f
        self.save_dir = save_dir
        self.start_epoch = 0
        self.best_acc1 = 0

        if pretrained:
            self.load()

    def _iteration(self,data_loader, is_train=True):
        loop_loss = []
        accuracy = []
        for data, target in data_loader:
            data, target = data.to(self.device), target.to(self.device)
            output = self.model(data)
            print(output.size())
            # loss = self.loss_f(output, target)
            data = data.view(-1, 784)
            loss = self.loss_f(output, data)
            loop_loss.append(loss.item()/len(data_loader))
            _, predict = torch.max(output.data, 1)
            accuracy.append((predict == target).sum().item())

            if is_train:
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
        loss = sum(loop_loss)
        acc = sum(accuracy)/len(data_loader.dataset)
        mode = 'train' if is_train else 'test'
        print(">>[{}] loss: {:.3f} accuracy: {:.2%}".format(mode,loss, acc))
        return loss, acc

    def train(self, data_loader):
        #self.model.train()
        with torch.enable_grad():
            loss, correct = self._iteration(data_loader, is_train=True)
            return correct

    def test(self, data_loader):
        self.model.eval()
        with torch.no_grad():
            loss, correct = self._iteration(data_loader, is_train=False)
            return correct

    def loop(self, epochs, train_data, test_data, scheduler= None):
        train_acc_list = []
        val_acc_list = []

        for ep in range(self.start_epoch, epochs + 1):
            if scheduler is not None:
                scheduler.step()

            print("epochs: {}".format(ep))
            train_acc = self.train(train_data)
            best_acc = self.test(test_data)

            train_acc_list.append(train_acc)
            val_acc_list.append(best_acc)

            if best_acc > self.best_acc1:
                self.best_acc1 = best_acc
                print("Best val acc till now: ",self.best_acc1)
                self.save(ep+1)

    def save(self, epoch):
        if self.save_dir is not None:

            state = {"epoch": epoch, "state_dict": self.model.state_dict(),
                     "optimizer": self.optimizer.state_dict(),
                     "best_acc1": self.best_acc1}

            torch.save(state, self.save_dir)
            print('model saved as {}'.format(self.save_dir))

    def load(self):
        checkpoint = torch.load(self.save_dir)
        self.model.load_state_dict(checkpoint['state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.start_epoch = checkpoint['epoch']
        self.best_acc1 = checkpoint['best_acc1']
